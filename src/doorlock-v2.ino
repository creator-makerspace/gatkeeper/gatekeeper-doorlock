#include <WiFiClientSecure.h>

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include <NeoPixelBus.h>

#include <Wire.h>
#include <PN532_I2C.h>
#include "PN532.h"

#define DEBUG true

// Use SSL or not
#define MQTT_SSL 0

#if MQTT_SSL == 1
#include "mqtt_crt.h"
#include "mqtt_key.h"
#endif

#include "time.h"

// Use Static IP - useful for development when you cant control DHCP for the CN / host field
const bool USE_STATIC = false;
const uint8_t IP[4] = {192, 168, 254, 15};
const uint8_t GATEWAY[4] = {192, 168, 254, 1};
const uint8_t SUBNET[4] = {255, 255, 255, 0};
const uint8_t DNS[4] = {8, 8, 8, 8};

// CONFIG HERE
// Set this to the identifier of the door
const char IDENTIFIER[37] = "door-test";

const char* WIFI_SSID = "SSID";
const char* WIFI_PASS = "PASS";

const char* MQTT_HOST = "10.0.0.6";
const char* MQTT_REQ_TOPIC = "commands";
const char* MQTT_RESP_TOPIC = "devices";
const int MQTT_RECONNECT_INTERVAL = 4000;

const int UPDATE_INTERVAL = 2000;
const int NFC_READ_INTERVAL = 200;

// Pin config
const int RELAY_PIN = D6;
const int LED_PIN = D5;
const int BUTTON_PIN = D3;
const int MAGNET_PIN = D7;
const int BUZZER_PIN = D4;

// DONT MODIFY BELOW

// Card / NFC variables
long lastRead = 0;
long lastSuccessRead = 0;

uint8_t lastCardUid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
uint8_t lastCardUidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)

// Network stuff
char msgBuffer[200];
int requestFailures = 0;
int requestFailureMillis = 0; // Time since the last verify failure
int lastConnAttempt = 0;

// General system vars
bool isOnline = false;
int connectState = -1;
int syncState = 0;
long timeSyncAt = 0;

// Track open timings
long openAt = 0;
long openDuration = 0;

// door open button
int buttonState;
int lastButtonState = LOW;
long lastDebounceTime = 0;
long debounceDelay = 50;

// Door open or closed status
int doorState = 0;

// Last status update
long lastStatusUpdate = 0;

// Neopixel status LED's
int systemLed = 0;
int readerLed = 1;
#define colorSaturation 128

int readerLedSetAt = 0;
int readerLedDuration = 500;

int buzzerAt = 0;
int buzzerDuration = 500;

// RGB Led / WS2812 objects
RgbColor red = RgbColor(colorSaturation, 0, 0);
RgbColor green = RgbColor(0, colorSaturation, 0);
RgbColor blue = RgbColor(0, 0, colorSaturation);
RgbColor white = RgbColor(colorSaturation);
RgbColor black = RgbColor(0);

NeoPixelBus statusLeds = NeoPixelBus(2, LED_PIN);

// Whether or not to use SSL / TLS
#if MQTT_SSL == 0
WiFiClient espClient;
#endif

#if MQTT_SSL == 1
WiFiClientSecure espClient;
#endif

PubSubClient client(espClient);

// NFC Reader object
PN532_I2C pn532_i2c(Wire);
PN532 nfc(pn532_i2c);


void callback(char* topic, byte* payload, unsigned int length);
void getUidChars(uint8_t uid[], uint8_t uidLength, char* result, const unsigned result_length);
void getDeviceId(char* identifier);
/**
   This routine turns off the I2C bus and clears it
   on return SCA and SCL pins are tri-state inputs.
   You need to call Wire.begin() after this to re-enable I2C
   This routine does NOT use the Wire library at all.

   returns 0 if bus cleared
           1 if SCL held low.
           2 if SDA held low by slave clock stretch for > 2sec
           3 if SDA held low after 20 clocks.
*/
int I2C_ClearBus() {
#if defined(TWCR) && defined(TWEN)
  TWCR &= ~(_BV(TWEN)); //Disable the Atmel 2-Wire interface so we can control the SDA and SCL pins directly
#endif
  pinMode(SDA, INPUT_PULLUP); // Make SDA (data) and SCL (clock) pins Inputs with pullup.
  pinMode(SCL, INPUT_PULLUP);

  delay(2500);  // Wait 2.5 secs. This is strictly only necessary on the first power
  // up of the DS3231 module to allow it to initialize properly,
  // but is also assists in reliable programming of FioV3 boards as it gives the
  // IDE a chance to start uploaded the program
  // before existing sketch confuses the IDE by sending Serial data.

  boolean SCL_LOW = (digitalRead(SCL) == LOW); // Check is SCL is Low.
  if (SCL_LOW) { //If it is held low Arduno cannot become the I2C master.
    return 1; //I2C bus error. Could not clear SCL clock line held low
  }

  boolean SDA_LOW = (digitalRead(SDA) == LOW);  // vi. Check SDA input.
  int clockCount = 20; // > 2x9 clock

  while (SDA_LOW && (clockCount > 0)) { //  vii. If SDA is Low,
    clockCount--;
    // Note: I2C bus is open collector so do NOT drive SCL or SDA high.
    pinMode(SCL, INPUT); // release SCL pullup so that when made output it will be LOW
    pinMode(SCL, OUTPUT); // then clock SCL Low
    delayMicroseconds(10); //  for >5uS
    pinMode(SCL, INPUT); // release SCL LOW
    pinMode(SCL, INPUT_PULLUP); // turn on pullup resistors again
    // do not force high as slave may be holding it low for clock stretching.
    delayMicroseconds(10); //  for >5uS
    // The >5uS is so that even the slowest I2C devices are handled.
    SCL_LOW = (digitalRead(SCL) == LOW); // Check if SCL is Low.
    int counter = 20;
    while (SCL_LOW && (counter > 0)) {  //  loop waiting for SCL to become High only wait 2sec.
      counter--;
      delay(100);
      SCL_LOW = (digitalRead(SCL) == LOW);
    }
    if (SCL_LOW) { // still low after 2 sec error
      return 2; // I2C bus error. Could not clear. SCL clock line held low by slave clock stretch for >2sec
    }
    SDA_LOW = (digitalRead(SDA) == LOW); //   and check SDA input again and loop
  }
  if (SDA_LOW) { // still low
    return 3; // I2C bus error. Could not clear. SDA data line held low
  }

  // else pull SDA line low for Start or Repeated Start
  pinMode(SDA, INPUT); // remove pullup.
  pinMode(SDA, OUTPUT);  // and then make it LOW i.e. send an I2C Start or Repeated start control.
  // When there is only one I2C master a Start or Repeat Start has the same function as a Stop and clears the bus.
  /// A Repeat Start is a Start occurring after a Start with no intervening Stop.
  delayMicroseconds(10); // wait >5uS
  pinMode(SDA, INPUT); // remove output low
  pinMode(SDA, INPUT_PULLUP); // and make SDA high i.e. send I2C STOP control.
  delayMicroseconds(10); // x. wait >5uS
  pinMode(SDA, INPUT); // and reset pins as tri-state inputs which is the default state on reset
  pinMode(SCL, INPUT);
  return 0; // all ok
}

void initI2C() {
  // Init I2C
  int rtn = I2C_ClearBus(); // clear the I2C bus first before calling Wire.begin()
  if (rtn != 0) {
    Serial.println(F("I2C bus error. Could not clear"));
    if (rtn == 1) {
      Serial.println(F("SCL clock line held low"));
    } else if (rtn == 2) {
      Serial.println(F("SCL clock line held low by slave clock stretch"));
    } else if (rtn == 3) {
      Serial.println(F("SDA data line held low"));
    }
  } else { // bus clear
    // re-enable Wire
    // now can start Wire Arduino master
    Wire.begin();

    // Try to slow down the clock for the reader to work more reialiably
    Wire.setClock(10000);
  }

  Serial.println("I2C init complete!");
}

void initNFC() {
  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    Serial.print("Didn't find PN53x board");
    while (1); // halt
  }
  // Got ok data, print it out!
  Serial.print("Found chip PN5"); Serial.println((versiondata >> 24) & 0xFF, HEX);
  Serial.print("Firmware ver. "); Serial.print((versiondata >> 16) & 0xFF, DEC);
  Serial.print('.'); Serial.println((versiondata >> 8) & 0xFF, DEC);

  // configure board to read RFID tags
  nfc.SAMConfig();

  Serial.println("NFC init complete!");
}

void setConnected() {
  if (connectState != WiFi.status()) {
    connectState = WiFi.status();
    if (connectState == WL_CONNECTED) {
      Serial.println("");
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
      Serial.println(WiFi.hostname());
    }
  }
}

void initWifi () {
  delay(50);

  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(WIFI_SSID);

  Serial.println(WiFi.macAddress());

  if (USE_STATIC) {
    WiFi.config(IP, GATEWAY, SUBNET, DNS);
  }
  WiFi.begin(WIFI_SSID, WIFI_PASS);
}

void setup(void) {
  char str[200];

  // Init Serial and wait 50..
  Serial.begin(115200);
  delay(50);

  configTime(0, 0, "ntp.uio.no", "time.nist.gov");

  Serial.setDebugOutput(true);

  char dev[17];
  getDeviceId(dev);

  // Set the hostname for cert / dhcp
  WiFi.hostname(dev);

  sprintf(str, "Starting %s device with point %s", dev, IDENTIFIER);
  Serial.println(str);

#if MQTT_SSL == 1
  Serial.println("SSL enabled for MQTT");
  espClient.setCertificate(esp_crt, esp_crt_len);
  espClient.setPrivateKey(esp_key, esp_key_len);
#endif

  // Sets LED to RED when it's bork
  statusLeds.Begin();
  statusLeds.SetPixelColor(systemLed, red);
  statusLeds.SetPixelColor(readerLed, red);
  statusLeds.Show();

  // Door solenoid pin
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);

  // Door opener button
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  // Magnet pin
  pinMode(MAGNET_PIN, INPUT_PULLUP);

  initWifi();

  initI2C();
  initNFC();

  client.setServer(MQTT_HOST, 1883);
  client.setCallback(callback);
}

void syncTime() {
  if ((syncState != 1 && (millis() - timeSyncAt) > 5000)) {
    if (!time(nullptr)) {
      timeSyncAt = millis();
    } else {
      syncState = 1;
      Serial.println("Time synced");
    }
  }
}

void pointActivate(int duration = 5000) {
  openAt = millis();
  openDuration = duration;
  digitalWrite(RELAY_PIN, HIGH);
}

void pointTimeout() {
  if ((millis() - openAt) > openDuration) {
    digitalWrite(RELAY_PIN, LOW);
  }
}

void setReaderLed(int duration = 600, RgbColor color = white) {
  statusLeds.SetPixelColor(readerLed, color);
  readerLedDuration = duration;
  readerLedSetAt = millis();
}

void readerLedTimeout() {
  if ((millis() - readerLedSetAt) > readerLedDuration) {
    statusLeds.SetPixelColor(readerLed, white);
  }

  statusLeds.Show();
}

void buzzerSet(int duration = 600) {
  digitalWrite(BUZZER_PIN, LOW);
  buzzerAt = millis();
  buzzerDuration = duration;
}

void buzzerTimeout() {
  if ((millis() - buzzerAt) > buzzerDuration) {
    digitalWrite(BUZZER_PIN, HIGH);
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  Serial.println("");

  char *cstring = (char *) payload; // NOT WORKING
  cstring[length]  = '\0';

  if (strcmp(cstring, "OPEN") == 0) {
    // Indicate that the door is opened
    pointActivate(8000);
    setReaderLed(1000, green);
    buzzerSet(1000);

    lastSuccessRead = 0;
  } else if (strcmp(cstring, "DENIED") == 0) {
    setReaderLed(1000, red);

    lastSuccessRead = 0;
  } else if (strcmp(cstring, "ACK_STATUS") == 0) {
  }
}

void reconnect() {
  char str[100];

  // Loop until we're reconnected
  if ((!client.connected()) && (millis() - lastConnAttempt) > MQTT_RECONNECT_INTERVAL) {
    sprintf(str, "Attempting to connect to %s", MQTT_HOST);
    Serial.println(str);
    lastConnAttempt = millis();

    char dev[17];
    getDeviceId(dev);
    if (client.connect("FOO")) {
      char topic[50];
      sprintf(topic, "%s/%s", MQTT_RESP_TOPIC, dev);
      client.subscribe(topic);

      isOnline = true;

      statusLeds.SetPixelColor(systemLed, white);
      statusLeds.SetPixelColor(readerLed, white);
      statusLeds.Show();

      Serial.println("Ready for work!");
    } else {
      isOnline = false;

      statusLeds.SetPixelColor(systemLed, red);
      statusLeds.SetPixelColor(readerLed, red);
      statusLeds.Show();

      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
    }
  }
}

void buttonOpen() {
  int reading = digitalRead(BUTTON_PIN);

  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:
    // if the button state has changed:

    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == LOW) {
        Serial.println("Opened via Button");
        pointActivate(8000);
      }
    }
  }

  lastButtonState = reading;
}

void credReadAndVerify() {
  Serial.println(lastSuccessRead);
  delay(200);

  if ((lastSuccessRead == 0) && (isOnline)) {
    uint8_t success;
    uint8_t uidLength;
    uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID

    // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
    // 'uid' will be populated with the UID, and uidLength will indicate
    // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
    success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength, 10);

    if (success) {
      int isSame = memcmp(lastCardUid, uid, uidLength);
      Serial.println("CARD READ");

      if (isSame == 0) {
        Serial.println("Same card detected, skipping");
        return;
      };

      lastSuccessRead = millis();

      memcpy(lastCardUid, uid, uidLength);

      // Display some basic information about the card
      Serial.println("Found an ISO14443A card");
      Serial.print("  UID Length: "); Serial.print(uidLength, DEC); Serial.println(" bytes");
      Serial.print("  UID Value: ");
      nfc.PrintHex(uid, uidLength);
      Serial.println("");

      if (uidLength == 4) {
        // We probably have a Mifare Classic card ...
        Serial.println("Seems to be a Mifare Classic card (4 byte UID)");

        // Now we need to try to authenticate it for read/write access
        // Try with the factory default KeyA: 0xFF 0xFF 0xFF 0xFF 0xFF 0xFF
        Serial.println("Trying to authenticate block 4 with default KEYA value");
        uint8_t keya[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

        // Start with block 4 (the first block of sector 1) since sector 0
        // contains the manufacturer data and it's probably better just
        // to leave it alone unless you know what you're doing
        success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength, 4, 0, keya);

        if (success) {
          Serial.println("Sector 1 (Blocks 4..7) has been authenticated");
          uint8_t data[16];

          // If you want to write something to block 4 to test with, uncomment
          // the following line and this text should be read back in a minute
          // data = { 'a', 'd', 'a', 'f', 'r', 'u', 'i', 't', '.', 'c', 'o', 'm', 0, 0, 0, 0};
          // success = nfc.mifareclassic_WriteDataBlock (4, data);

          // Try to read the contents of block 4
          success = nfc.mifareclassic_ReadDataBlock(4, data);

          if (success) {
            // Data seems to have been read ... spit it out
            Serial.println("Reading Block 4:");
            nfc.PrintHexChar(data, 16);
            Serial.println("");

            // Wait a bit before reading the card again
          } else {
            Serial.println("Ooops ... unable to read the requested block.  Try another key?");
          }
        } else {
          Serial.println("Ooops ... authentication failed: Try another key?");
        }
      }

      if (uidLength == 7) {
        // We probably have a Mifare Ultralight card ...
        Serial.println("Seems to be a Mifare Ultralight tag (7 byte UID)");

        // Try to read the first general-purpose user page (#4)
        Serial.println("Reading page 4");
        uint8_t data[32];
        success = nfc.mifareultralight_ReadPage (4, data);
        if (success) {
          // Data seems to have been read ... spit it out
          nfc.PrintHexChar(data, 4);
          Serial.println("");
        } else {
          Serial.println("Ooops ... unable to read the requested page!?");
        }
      }

      setReaderLed(1000, blue);

      char id[100] = {};
      int cardIdLen = 0;
      char req[400] = {};
      getUidChars(uid, uidLength, id, cardIdLen);

      char dev[17] = {};
      getDeviceId(dev);

      sprintf(req, "VERIFY;point=%s,device=%s,credential=%s", IDENTIFIER, dev, id);
      client.publish(MQTT_REQ_TOPIC, req);
      Serial.println(req);
    } else {
      Serial.println("FAILED TO READ");
      lastCardUid[0] = 0;
    }
  }
}

void checkVerifyTimedOut() {
  // If we dont get a response within 5 seconds we fail the current request...
  if ((lastSuccessRead != 0) && (isOnline)) {
    if ((millis() - lastSuccessRead) > 1000) {
      Serial.println("Failed verification");
      statusLeds.SetPixelColor(readerLed, red);
      statusLeds.Show();

      requestFailureMillis = millis();
      requestFailures++;
      isOnline = false;
    }
  }

  if ((requestFailureMillis != 0) && (millis() - requestFailureMillis) > 2000) {
    statusLeds.SetPixelColor(readerLed, white);
    statusLeds.Show();

    isOnline = true;
    lastSuccessRead = 0;
    requestFailureMillis = 0;
    lastCardUid[0] = 0;
  }
}

/*
   Handles updates about the door states
*/
void updateDoorStatus() {
  doorState = digitalRead(MAGNET_PIN);
}

/*
   Post status about Door closed state etc to the controller via MQTT.
*/
void postStatus() {
  char buf[100] = {};

  char dev[17] = {};
  getDeviceId(dev);

  if ((millis() - lastStatusUpdate) > UPDATE_INTERVAL) {
    sprintf(buf, "UPDATE_STATUS;open=%d,device=%s,point=%s", doorState, dev, IDENTIFIER);
    client.publish(MQTT_REQ_TOPIC, buf);
    lastStatusUpdate = millis();
  }
}

void getUidChars(uint8_t uid[], uint8_t uidLength, char* result, const unsigned result_length) {
  char part[] = {};
  for (int i = 0; i < uidLength; i++) {
    sprintf(part, "%02x", uid[i]);
    strcat(result, part);
  }
}

void stripChars(char *str, char garbage) {

  char *src, *dst;
  for (src = dst = str; *src != '\0'; src++) {
    *dst = *src;
    if (*dst != garbage) dst++;
  }
  *dst = '\0';
}

void getDeviceId(char* id) {
  if (id) {
    sprintf(id, "esp-ctrl-%s", WiFi.macAddress().c_str());
    stripChars(id, ':');

    for (int i = 0; id[i]; i++) {
      id[i] = tolower(id[i]);
    }
  }
}

void loop() {
  pointTimeout();
  readerLedTimeout();
  buzzerTimeout();
  // Open via button (Done on the inside of the door)
  buttonOpen();

  setConnected();
  syncTime();

  if ((connectState == WL_CONNECTED) && (syncState == 1)) {
    if (!client.connected()) {
      reconnect();
      return;
    }

    if ((millis() - lastRead) > NFC_READ_INTERVAL) {
      credReadAndVerify();
      lastRead = millis();
    }

    checkVerifyTimedOut();

    updateDoorStatus();
    postStatus();

    client.loop();
  }
}

